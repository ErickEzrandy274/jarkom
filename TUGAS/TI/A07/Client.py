#!/usr/bin/env python

import socket

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad, unpad
from nacl.public import PrivateKey, SealedBox, PublicKey

SERVER_IP = "34.68.129.177"
SERVER_PORT = 5892
BUFFER_SIZE = 2048
BLOCK_SIZE = 32


def generate_asymmetric_key():
    private_key = PrivateKey.generate()
    file_out = open("PrivateKeyClient.pem", "wb")
    file_out.write(bytes(private_key))
    file_out.close()

    public_key = private_key.public_key
    file_out = open("PublicKeyClient.pem", "wb")
    file_out.write(bytes(public_key))
    file_out.close()


def generate_symmetric_key():
    sym_key = get_random_bytes(BLOCK_SIZE)
    file_out = open("SymmetricKeyClient.txt", "wb")
    file_out.write(sym_key)
    file_out.close()


def get_symmetric_object(key):
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher


def encrypt_with_symmetric(cipher, message):
    result = cipher.encrypt(pad(message.encode(), BLOCK_SIZE))
    return result


def encrypt_with_asymmetric(public_key, message):
    box = SealedBox(PublicKey(public_key))
    result = box.encrypt(message)
    return result


def decrypt_with_symmetric(cipher, message):
    result = unpad(cipher.decrypt(message), BLOCK_SIZE)
    return result


def decrypt_with_asymmetric(private_key, message):
    box = SealedBox(PrivateKey(private_key))
    result = box.decrypt(message)
    return result


def encode_utf8_before_send(message):
    return message.encode("UTF-8")


def decode_utf8_before_print(message):
    return message.decode("UTF-8")


# This method loads the key to the program or generate a new one if freshly started.
def setup(type="fresh"):
    if type == "fresh":
        generate_asymmetric_key()
        generate_symmetric_key()

    symmetric_key = open("SymmetricKeyClient.txt", "rb")
    symmetric_key = symmetric_key.read()
    privkey = open("PrivateKeyClient.pem", "rb")
    privkey = privkey.read()
    pubkey = open("PublicKeyClient.pem", "rb")
    pubkey = pubkey.read()
    symmetric_key_object = get_symmetric_object(symmetric_key)
    return symmetric_key, privkey, pubkey, symmetric_key_object


def connect_symmetric():
    # change this to empty/fresh if you are running it first time
    symmetric_key, privkey, pubkey, symmetric_key_object = setup("rerun")
    sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sc.connect((SERVER_IP, SERVER_PORT))

    #* Input pertama
    input_value = input("Input_1 untuk dikirimkan ke server: ")
    sc.send(encode_utf8_before_send(input_value))

    #* Output pertama
    output_value_bytes = sc.recv(BUFFER_SIZE)
    output_value = decode_utf8_before_print(output_value_bytes)
    print(f"Balasan dari server {(SERVER_IP, SERVER_PORT)}: {output_value}")

    #* Input kedua
    input_value_2 = input("Input_2 untuk dikirimkan ke server: ") 
    sc.send(encode_utf8_before_send(input_value_2))
    sc.send(symmetric_key)

    #* Output kedua
    output_value_bytes_2 = sc.recv(BUFFER_SIZE)
    decript_message_2 = decrypt_with_symmetric(symmetric_key_object, output_value_bytes_2)
    output_value_2 = decode_utf8_before_print(decript_message_2)
    print(f"Balasan dari server {(SERVER_IP, SERVER_PORT)}: {output_value_2}")

    #* Input ketiga
    raw_input_value_3 = input("Input_3 untuk dikirimkan ke server: ") 
    input_value_3 = f"{raw_input_value_3} 2006595892"
    encrypt_message_3 = encrypt_with_symmetric(symmetric_key_object, input_value_3)
    sc.send(encrypt_message_3)

    #* Output ketiga
    output_value_bytes_3 = sc.recv(BUFFER_SIZE)
    decript_message_3 = decrypt_with_symmetric(symmetric_key_object, output_value_bytes_3)
    output_value_3 = decode_utf8_before_print(decript_message_3)
    print(f"Balasan dari server {(SERVER_IP, SERVER_PORT)}: {str(output_value_3)}")

    sc.close()


def connect_asymmetric():
    # change this to empty/fresh if you are running it first time
    symmetric_key, privkey, pubkey, symmetric_key_object = setup("rerun")
    sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sc.connect((SERVER_IP, SERVER_PORT))

    #* Input pertama
    input_value = input("Input_1 untuk dikirimkan ke server: ")
    sc.send(encode_utf8_before_send(input_value))

    #* Output pertama
    output_value_bytes = sc.recv(BUFFER_SIZE)
    output_value = decode_utf8_before_print(output_value_bytes)
    print(f"Balasan dari server {(SERVER_IP, SERVER_PORT)}: {output_value}")

    #* Program Client mengirim Public Keynya ke server.
    sc.send(pubkey)

    #* Program Client menampilkan public key server yang diterima
    pubkey_server = sc.recv(BUFFER_SIZE)
    print(f"Public Key Server berupa {pubkey_server}")

    #* Program client mengenkripsi Symmetric Keynya dengan Public Key milik Program server
    client_symkey_encrypt = encrypt_with_asymmetric(pubkey_server, symmetric_key)
    sc.send(client_symkey_encrypt)

    #* Program client mendekripsi pesan server
    response_encrypt = sc.recv(BUFFER_SIZE)
    response = decrypt_with_symmetric(symmetric_key_object, response_encrypt)
    print(f"Balasan dari server {(SERVER_IP, SERVER_PORT)}: {decode_utf8_before_print(response)}")

     #* Input ketiga
    input_value_3 = input("Input_3 untuk dikirimkan ke server: ") 
    encrypt_message_3 = encrypt_with_symmetric(symmetric_key_object, input_value_3)
    sc.send(encrypt_message_3)

    #* Output ketiga
    raw_response_3 = sc.recv(BUFFER_SIZE)
    print(f"Menerima balasan dari {(SERVER_IP, SERVER_PORT)} berupa pesan yang sudah dienkripsi: {raw_response_3}")
    response_3 = decrypt_with_symmetric(symmetric_key_object, raw_response_3)
    print(f"Balasan dari {(SERVER_IP, SERVER_PORT)} yang sudah didekripsi berupa: {decode_utf8_before_print(response_3)}")

    sc.close()


def main():
    print("Starting Client Program.")
    # ganti method connect ke encryption yang sedang dikerjakan
    connect_asymmetric()


if __name__ == "__main__":
    main()
