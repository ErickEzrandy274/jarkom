#!/usr/bin/env python

import socket
import threading
from typing import Tuple

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad, unpad
from nacl.public import PrivateKey, SealedBox, PublicKey

SERVER_NAME = ""
SERVER_IP = "34.68.129.177"
SERVER_PORT = 5892
BUFFER_SIZE = 2048
BLOCK_SIZE = 32


def generate_asymmetric_key():
    private_key = PrivateKey.generate()
    file_out = open("PrivateKeyServer.pem", "wb")
    file_out.write(bytes(private_key))
    file_out.close()

    public_key = private_key.public_key
    file_out = open("PublicKeyServer.pem", "wb")
    file_out.write(bytes(public_key))
    file_out.close()


def generate_symmetric_key():
    sym_key = get_random_bytes(BLOCK_SIZE)
    file_out = open("SymmetricKeyServer.txt", "wb")
    file_out.write(sym_key)
    file_out.close()


def get_symmetric_object(key):
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher


def encrypt_with_symmetric(cipher, message):
    result = cipher.encrypt(pad(message.encode(), BLOCK_SIZE))
    return result


def encrypt_with_asymmetric(public_key, message):
    box = SealedBox(PublicKey(public_key))
    result = box.encrypt(message)
    return result


def decrypt_with_symmetric(cipher, message):
    result = unpad(cipher.decrypt(message), BLOCK_SIZE)
    return result


def decrypt_with_asymmetric(private_key, message):
    box = SealedBox(PrivateKey(private_key))
    result = box.decrypt(message)
    return result


def encode_utf8_before_send(message):
    return message.encode("UTF-8")


def decode_utf8_before_print(message):
    return message.decode("UTF-8")


# This method loads the key to the program or generate a new one if freshly started.
def setup(type="fresh"):
    if (type == "fresh"):
        generate_asymmetric_key()
        generate_symmetric_key()

    symmetric_key = open("SymmetricKeyServer.txt", "rb")
    symmetric_key = symmetric_key.read()
    privkey = open("PrivateKeyServer.pem", "rb")
    privkey = privkey.read()
    pubkey = open("PublicKeyServer.pem", "rb")
    pubkey = pubkey.read()
    symmetric_key_object = get_symmetric_object(symmetric_key)
    return symmetric_key, privkey, pubkey, symmetric_key_object


def socket_handler_symmetric(connection: socket.socket, address: Tuple[str, int]):
    print(f"Receive connection from {address}")

    #* Input pertama
    input_value_bytes = connection.recv(BUFFER_SIZE)
    input_value = decode_utf8_before_print(input_value_bytes)
    print(f"Menerima input dari {address} berupa {input_value}")

    #* Output pertama
    connection.send(encode_utf8_before_send("Halo juga Client!"))

    #* Input kedua
    input_value_bytes_2 = connection.recv(BUFFER_SIZE)
    sym_key = connection.recv(BUFFER_SIZE)
    input_value_2 = decode_utf8_before_print(input_value_bytes_2)
    print(f"Menerima input dari {address} berupa symmetric key client: {sym_key}")

    #* Output kedua
    cipher = get_symmetric_object(sym_key)
    result = encrypt_with_symmetric(cipher, "Symmetric Key diterima!")
    connection.send(result)

    #* Input ketiga
    input_value_bytes_3 = connection.recv(BUFFER_SIZE)
    raw_input_value_3 = decrypt_with_symmetric(cipher, input_value_bytes_3)
    input_value_3 = decode_utf8_before_print(raw_input_value_3)
    npm_client = input_value_3[input_value_3.rindex(" ") + 1:]
    print(f"Menerima input dari {address} berupa NPM Client: {npm_client}")

    #* Output ketiga
    result_3 = encrypt_with_symmetric(cipher, f"Terima pesan {npm_client}!")
    connection.send(result_3)

    connection.close()


def socket_handler_asymmetric(connection: socket.socket, address: Tuple[str, int]):
    symmetric_key, privkey, pubkey, symmetric_key_object = setup("rerun")
    print(f"Receive connection from {address}")

    #* Input pertama
    input_value_bytes = connection.recv(BUFFER_SIZE)
    input_value = decode_utf8_before_print(input_value_bytes)
    print(f"Menerima input dari {address} berupa {input_value}")

    #* Output pertama
    connection.send(encode_utf8_before_send("Halo juga Instance Client!"))

    #* Program server menampilkan public key client yang diterima
    pubkey_client = connection.recv(BUFFER_SIZE)
    print(f"Public Key Client berupa {pubkey_client}")

    #* Program Server mengirim Public Keynya ke client
    connection.send(pubkey)

    #* Program server mendekripsi Symmetric Key yang diterima dengan Private Keynya
    client_symkey_encrypt = connection.recv(BUFFER_SIZE)
    client_symkey = decrypt_with_asymmetric(privkey, client_symkey_encrypt)
    cipher = get_symmetric_object(client_symkey)
    connection.send(encrypt_with_symmetric(cipher, "Symmetric Key diterima!"))

    #* Input ketiga
    input_value_bytes_3 = connection.recv(BUFFER_SIZE)
    print(f"Menerima input dari {address} berupa pesan yang sudah dienkripsi: {input_value_bytes_3}")
    input_value_3 = decode_utf8_before_print(decrypt_with_symmetric(cipher, input_value_bytes_3))
    npm_client = input_value_3[input_value_3.rindex(" ") + 1:]
    print(f"Input dari {address} yang sudah didekripsi berupa: {input_value_3}")

    #* Output ketiga
    result_3 = encrypt_with_symmetric(cipher, f"Terima pesan {npm_client}!")
    connection.send(result_3)

    connection.close()


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sc:
        sc.bind((SERVER_NAME, SERVER_PORT))
        sc.listen(0)

        print("Multithreading Socket Server Program")
        print("Hit Ctrl+C to terminate the program")

        while True:
            connection, address = sc.accept()
            # ganti handler ke tipe socket yang sedang dikerjakan
            thread = threading.Thread(target=socket_handler_asymmetric, args=(connection, address))
            thread.start()


if __name__ == "__main__":
    main()
