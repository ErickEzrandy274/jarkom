enable
configure terminal
ip nat pool pool_rizelick 200.16.24.0 200.16.24.63 netmask 255.255.0.0
access-list 1 permit 10.24.12.0 0.0.0.3
access-list 1 permit 10.24.13.0 0.0.0.3
access-list 1 permit 10.24.14.0 0.0.0.3
access-list 1 permit 10.24.15.0 0.0.0.3
ip nat inside source list 1 pool pool_rizelick
int se1/0
ip nat inside
exit
int se1/1
ip nat inside
exit
int eth0/0/0
ip nat outside
end
int fa0/0
ip nat outside
end
int fa0/1
ip nat outside
end
