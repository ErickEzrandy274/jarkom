TOL = 10**-5
import math

def G1(x):
  prevRes = (x**2)-3
  currentRes = (prevRes**2)-3

  counter = 1
  while(abs(currentRes-prevRes) > TOL):
    prevRes = currentRes
    currentRes = (prevRes**2)-3
    counter += 1
  print(f"currentRes: {currentRes}, counter: {counter}")

def G2(x):
  prevRes = math.sqrt(x+3)
  currentRes = math.sqrt(prevRes+3)
  counter = 1

  while(abs(currentRes-prevRes) > TOL):
    prevRes = currentRes
    currentRes = math.sqrt(prevRes+3)
    counter += 1
  print(f"currentRes: {currentRes}, counter: {counter}")

def G3(x):
  prevRes = 1+(3/x)
  currentRes = 1+(3/prevRes)
  counter = 1

  while(abs(currentRes-prevRes) > TOL):
    prevRes = currentRes
    currentRes = 1+(3/prevRes)
    counter += 1
  print(f"currentRes: {currentRes}, counter: {counter}")

# G1(1)
G2(1)
# G3(1)