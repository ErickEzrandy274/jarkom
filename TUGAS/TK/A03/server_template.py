import socket
import threading
import time
from typing import Tuple

#belum tentu kepake
BUFFER_SIZE = 2048
FORWARDING_SERVER_IP = ""
FORWARDING_SERVER_PORT = 6024
SERVER_IP_ASDOS = "34.101.92.60" 
SERVER_PORT_ASDOS = 5353


def translate(message):
    id = str(int(message[:4], 16))
    flags = str(bin(int(message[4:8], 16))[2:].zfill(16))
    qr = str(int(str(hex(int(flags[0]))[2:])))
    opc = int(flags[1:5], 2)
    aa = str(int(str(hex(int(flags[5]))[2:])))
    tc = str(int(str(hex(int(flags[6]))[2:])))
    rd = str(int(str(hex(int(flags[7]))[2:])))
    ra = str(int(str(hex(int(flags[8]))[2:])))
    ad = str(int(str(hex(int(flags[10]))[2:])))
    cd = str(int(str(hex(int(flags[11]))[2:])))
    rcode = int(flags[12:], 2)
    print("id: ", id)
    print("flags: ", flags)
    print("qr: ", qr)
    print("opc: ", opc)
    print("aa: ", aa)
    print("tc: ", tc)
    print("rd: ", rd)
    print("ra: ", ra)
    print("ad: ", ad)
    print("cd: ", cd)
    print("rcode: ", rcode)
    qdcount = int(str(bin(int(message[8:12], 16))[2:].zfill(16)), 2)
    ancount = int(str(bin(int(message[12:16], 16))[2:].zfill(16)), 2)
    nscount = int(str(bin(int(message[16:20], 16))[2:].zfill(16)), 2)
    arcount = int(str(bin(int(message[20:24], 16))[2:].zfill(16)), 2)
    others = message[24:]
    print("qdcount: ", qdcount)
    print("ancount: ", ancount)
    print("nscount: ", nscount)
    print("arcount: ", arcount)
    print("others: ", others)
    list_response = [id, qr, opc, aa, tc, rd, ra, ad, cd, rcode, qdcount, ancount, nscount, arcount, others]
    
    return list_response

def decrypt(list_response, addr):
    list_question = []
    question = list_response[len(list_response)-1]
    loop = int(question[:2])
    qname = ''
    index = 2
    index_loop = 0

    while loop != 0:
        qname += bytearray.fromhex(question[index:index + 2*loop]).decode() + "."
        index_loop += 2*(loop+1)
        loop = int(question[index_loop:index_loop+2])
        index = index_loop + 2

    qtype = question[index:index+4]
    qclass = question[index+4:index+8]
    list_question.append(qname[:-1])
    list_question.append(str(int(qtype, 16)))
    list_question.append(str(int(qclass, 16)))

    result = f"""
        =========================================================================\n
        [Request from ('{addr}', {PORT})]\n
        -------------------------------------------------------------------------\n
        HEADERS\n
        Request ID: {list_response[0]}\n
        QR: {list_response[1]} | OPCODE: {list_response[2]} | AA: {list_response[3]} | TC: {list_response[4]} | RD: {list_response[5]} | RA: {list_response[6]} | AD: {list_response[7]} | CD: {list_response[8]} | RCODE: {list_response[9]}\n
        Question Count: {list_response[10]} | Answer Count: {list_response[11]} | Authority Count: {list_response[12]} | Additional Count: {list_response[13]}\n
        -------------------------------------------------------------------------\n
        QUESTION\n
        Domain Name: {list_question[0]} | QTYPE: {list_question[1]} | QCLASS: {list_question[2]}\n
        -------------------------------------------------------------------------\n
    """
    return result

def request_parser(request_message_raw: bytearray, source_address: Tuple[str, int]) -> str:
    inbound_message = request_message_raw.hex()
    print("inbound_message: ", inbound_message)
    print("request_message_raw: ", request_message_raw)
    return decrypt(translate(inbound_message), source_address)

    
    # Put your request message decoding logic here.
    # This method return a str.
    # Anda boleh menambahkan helper fungsi/method sebanyak yang Anda butuhkan selama 
    # TIDAK MENGUBAH ATAUPUN MENGHAPUS SPEC (parameter dan return type) YANG DIMINTA.

def response_parser(response_mesage_raw: bytearray) -> str:
    # Put your request message decoding logic here.
    # This method return a str.
    # Anda boleh menambahkan helper fungsi/method sebanyak yang Anda butuhkan selama 
    # TIDAK MENGUBAH ATAUPUN MENGHAPUS  SPEC (parameter dan return type) YANG DIMINTA.
    ID, QR, OPCODE, AA, TC, RD, RA, AD, CD, RCODE, QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT, QNAME, QTYPE, QCLASS, TYPE, CLASS, TTL, RDLENGTH, RDATA = ""
    result = f"""
        =========================================================================\n
        [Response from DNS Server]\n
        -------------------------------------------------------------------------\n
        HEADERS\n
        Request ID: {ID}\n
        QR: {QR} | OPCODE: {OPCODE} | AA: {AA} | TC: {TC} | RD: {RD} | RA: {RA} | AD: {AD} | CD: {CD} | RCODE: {RCODE}\n
        Question Count: {QDCOUNT} | Answer Count: {ANCOUNT} | Authority Count: {NSCOUNT} | Additional Count: {ARCOUNT}\n
        -------------------------------------------------------------------------\n
        QUESTION\n
        Domain Name: <QNAME> | QTYPE: <QTYPE> | QCLASS: <QCLASS>\n
        -------------------------------------------------------------------------\n
        ANSWER\n
        TYPE: <TYPE> | CLASS: <CLASS> | TTL: <TTL> | RDLENGTH: <RDLENGTH>\n
        IP Address: <RDATA>\n
        ==========================================================================\n

    """
    return result

def socket_handler(sc: socket.socket, inbound_message_raw: bytearray, source_addr: Tuple[str, int]):
    # Menerima request dari user
    print(request_parser(inbound_message_raw, source_addr))

    # Meneruskan pesan ke server dns asdos
    dns_asdos = (SERVER_IP_ASDOS, SERVER_PORT_ASDOS)
    sc.sendto(inbound_message_raw, dns_asdos)

    # Menerima pesan balasan dan mengirimkan ke user
    inbound_message_raw, dns_address = sc.recvfrom(BUFFER_SIZE)
    sc.sendto(inbound_message_raw, source_addr)

def main():
    # Put the rest of your program's logic here (socket etc.). 
    # Pastikan blok socket Anda berada pada fungsi ini.

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as server_socket:
        server_socket.bind((FORWARDING_SERVER_IP, FORWARDING_SERVER_PORT))
        while True:
            inbound_message_raw, source_addr = server_socket.recvfrom(BUFFER_SIZE)
            socket_handler(server_socket, inbound_message_raw, source_addr)


# DO NOT ERASE THIS PART!
if __name__ == "__main__":
    main() 