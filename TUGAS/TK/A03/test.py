def decrypt(list_response, addr):
    list_question = []
    question = list_response[len(list_response)-1]
    loop = int(question[:2], 16)
    print(loop)
    qname = ''
    index = 2
    index_loop = 0

    while loop != 0:
        qname += bytearray.fromhex(question[index:index + 2*loop]).decode() + "."
        index_loop += 2*(loop+1)
        loop = int(question[index_loop:index_loop+2], 16)
        index = index_loop + 2

    qtype = question[index:index+4]
    qclass = question[index+4:index+8]
    list_question.append(qname[:-1])
    list_question.append(str(int(qtype, 16)))
    list_question.append(str(int(qclass, 16)))
    return list_question

a = "keiyo.jarkomdat.csui".encode("utf-8")
message = a.hex()

header = str(bin(int(message[0:24], 16)))[2:98]
print(header)

id = int(header[:16], 2)
qr = int(header[16], 2)
opcode = int(header[17:21], 2)
aa = int(header[21], 2)
tc = int(header[22], 2)
rd = int(header[23], 2)
ra = int(header[24], 2)
ad = int(header[26], 2)
cd = int(header[27], 2)

rcode = int(header[28:32], 2)
qdcount = int(header[32:48], 2)
ancount = int(header[48:64], 2)
nscount = int(header[64:80], 2)
arcount = int(header[80:], 2)
others = message[24:]
list_response = [id, qr, opcode, aa, tc, rd, ra, ad, cd, rcode, qdcount, ancount, nscount, arcount, others]
print(f"others is {others}")

print(f"list response: {list_response}")
# print(list_response ('DUMMMY')))